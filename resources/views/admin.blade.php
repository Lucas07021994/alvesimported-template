<!DOCTYPE html>
<html dir="ltr" lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/assets/images/favicon.png">
    <title></title>
    <!-- Custom CSS -->
    <link href="/assets/assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="/assets/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="/assets/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="/assets/dist/css/style.min.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
     data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
    <header class="topbar" data-navbarbg="skin6">
        @include('_partials.navbar')
    </header>
    <aside class="left-sidebar" data-sidebarbg="skin6">
        <div class="scroll-sidebar" data-sidebarbg="skin6">
            @include('_partials.menu')
        </div>
    </aside>
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-truncate text-dark font-weight-medium mb-1"></h3>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb m-0 p-0">
                                <li class="breadcrumb-item"><a href="">Dashboard</a>
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            @yield('content')
        </div>
        <footer class="footer text-center text-muted">
            All Rights Reserved by Adminmart. Designed and Developed by <a
                    href="https://wrappixel.com">WrapPixel</a>.
        </footer>
    </div>
</div>
<script src="/assets/assets/libs/jquery/dist/jquery.min.js"></script>
<script src="/assets/assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="/assets/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- apps -->
<!-- apps -->
<script src="/assets/dist/js/app-style-switcher.js"></script>
<script src="/assets/dist/js/feather.min.js"></script>
<script src="/assets/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="/assets/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="/assets/dist/js/custom.min.js"></script>
<!--This page JavaScript -->
<script src="/assets/assets/extra-libs/c3/d3.min.js"></script>
<script src="/assets/assets/extra-libs/c3/c3.min.js"></script>
<script src="/assets/assets/libs/chartist/dist/chartist.min.js"></script>
<script src="/assets/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
<script src="/assets/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
<script src="/assets/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
<script src="/assets/dist/js/pages/dashboards/dashboard1.min.js"></script>
</body>

</html>
