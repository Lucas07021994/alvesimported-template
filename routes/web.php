<?php

use App\Http\Controllers\Site\HomeController;
use App\Routes\Navbar\NavbarRoute;
use Illuminate\Support\Facades\Route;



Route::get('/',[HomeController::class, 'index']);


Route::get('/cursos', function () {
    return view('site.courses');
})->name('site.courses');

Route::get('/contato', function () {
    return view('site.contact');
})->name('site.contact');

NavbarRoute::routes();
